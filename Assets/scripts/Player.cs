﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public Agent agent;
	
	void Awake() {
		agent = GetComponent<Agent>();
	}

	void Start() {
	}
	
	void Update() {
		Globals.PlayerKilledAgentTime -= Time.deltaTime;
		agent.move.MoveDx = Input.GetAxis("Horizontal");
		agent.move.DoJump = Input.GetButton("Jump");
		agent.move.UseRun = Input.GetKey(KeyCode.LeftShift);
		agent.carry.DoThrow = false;
		agent.carry.DoPickUp = false;
		if(Input.GetButton("Fire3")) {
			if(agent.carry.IsCarrying) {
				agent.carry.DoThrow = true;
				float q = Input.GetAxis("Vertical");
				if(q > 0.1f) agent.carry.ThrowMode = +1;
				else if(q < -0.1f) agent.carry.ThrowMode = -1;
				else agent.carry.ThrowMode = 0;
			}
			else 
				agent.carry.DoPickUp = true;
		}
		agent.slice.DoAttack = Input.GetButton("Fire1");

		agent.health.Health += Time.deltaTime*0.1f;
	}
}
